import { CcarUiPage } from './app.po';

describe('ccar-ui App', () => {
  let page: CcarUiPage;

  beforeEach(() => {
    page = new CcarUiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
