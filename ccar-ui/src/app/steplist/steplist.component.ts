import {Component, Inject, Input, OnInit, TemplateRef} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {KieServerConfig} from "../kieserver.config";
import {Step} from "./step";
import {ActionConfig, ListConfig, ListEvent} from "patternfly-ng";

const httpheaders:HttpHeaders = new HttpHeaders().set('Accept','application/json').set('Content-Type','application/json');

@Component({
  selector: 'app-steplist',
  templateUrl: './steplist.component.html',
  styleUrls: ['./steplist.component.css']
})
export class SteplistComponent implements OnInit {

  @Input() scenarioId: string;
  steps: Step[] = [];
  listConfig: ListConfig;



  constructor(private http: HttpClient, @Inject('KieServerConfig') private kieServerConfig: KieServerConfig) {
  }

  ngOnInit() {
    var baseurl = '/kie-server/services/rest/server/containers/' + this.kieServerConfig.containerId + '/processes/definitions/';
    this.http.get(baseurl + this.scenarioId, {headers: httpheaders})
      .subscribe(data=>{
        data['process-subprocesses'].forEach(s=>{
          this.http.get(baseurl + s, {headers: httpheaders})
            .subscribe(r=>{
              this.addStep({name: r['process-name'], id:r['process-id']} as Step);
            });
        });

      });


    this.listConfig = {
      dblClick: false,
      multiSelect: false,
      selectItems: true,
      selectionMatchProp: 'name',
      showCheckbox: false,
      useExpandItems: true
    } as ListConfig;
  }

  addStep(s:Step){
    this.steps.push(s);

    this.steps.sort(function (a, b) {
      function getNumber(s) {
        var index = -1;
        ['one', 'two', 'three', 'four', 'five', 'six'].some(function (c, i) {
          if (~s.name.indexOf(c)) {
            index = i;
            return true;
          }
        });
        return index;
      }
      return getNumber(a) - getNumber(b);
    });

  }

  getActionConfig(item: any, startButtonTemplate: TemplateRef<any>): ActionConfig {
    let actionConfig = {
      primaryActions: [{
        id: 'start',
        styleClass: 'btn-primary',
        title: 'Start',
        tooltip: 'Start the process',
        template: startButtonTemplate
      }]
    }as ActionConfig
    return actionConfig;
  }

  handleAction($event: ListEvent) {

  }

  handleCheckboxChange($event: ListEvent) {

  }

  handleClick($event: ListEvent) {

  }

  handleDblClick($event: ListEvent) {

  }

  handleSelect($event: ListEvent) {

  }

  handleSelectionChange($event: ListEvent) {

  }

}
