import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessimageComponent } from './processimage.component';

describe('ProcessimageComponent', () => {
  let component: ProcessimageComponent;
  let fixture: ComponentFixture<ProcessimageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessimageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessimageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
