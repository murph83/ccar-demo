import {Component, Inject, Input, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {KieServerConfig} from "../kieserver.config";

@Component({
  selector: 'app-processimage',
  templateUrl: './processimage.component.html',
  styleUrls: ['./processimage.component.css']
})
export class ProcessimageComponent implements OnInit {

  processImage: SafeResourceUrl;
  @Input() processId: string;
  @Input() processInstanceId?: string;

  constructor(@Inject('KieServerConfig')private kieServerConfig:KieServerConfig, private sanitizer:DomSanitizer) { }

  ngOnInit() {
    var url = '/kie-server/services/rest/server/containers/'+ this.kieServerConfig.containerId +'/images/processes/' + this.processId;
    if (this.processInstanceId){
      url += '/instances/' + this.processInstanceId;
    }

    this.processImage = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
