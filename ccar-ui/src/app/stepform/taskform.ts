export interface DataHolder {
  "id": string;
  "inputId": string;
  "name": string;
  "outId": string;
  "type": string;
  "value": string;
}

export interface Field {
  "errorMessage": string;
  "fieldClass": string;
  "fieldRequired": boolean;
  "hideContent": boolean;
  "id": number;
  "inputBinding": string;
  "isHTML": boolean;
  "label": string;
  "name": string;
  "outputBinding": string;
  "position": number;
  "readonly": boolean;
  "title": string;
  "type": string;
}

export interface TaskForm {

  "dataHolder": DataHolder[];
  "displayMode": string;
  "field": Field[];
  "id": number;
  "name": string;
  "status": number;


}
