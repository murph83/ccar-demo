import {Component, Inject, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {KieServerConfig} from "../kieserver.config";
import {TaskForm} from "app/stepform/taskform";

@Component({
  selector: 'app-stepform',
  templateUrl: './stepform.component.html',
  styleUrls: ['./stepform.component.css']
})
export class StepformComponent implements OnInit {

  @Input() stepId: string;
  form: TaskForm;

  constructor(private http: HttpClient, @Inject('KieServerConfig') private kieServerConfig: KieServerConfig) {
  }

  ngOnInit() {
    this.http.get('/kie-server/services/rest/server/containers/'+ this.kieServerConfig.containerId +'/forms/processes/' + this.stepId,
      {headers: new HttpHeaders().set('Accept','application/json').set('Content-Type','application/json')})
      .subscribe(data => {
        this.form = data['form'] as TaskForm;
      });
  }

}
