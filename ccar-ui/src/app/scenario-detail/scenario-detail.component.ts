import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import 'rxjs/add/operator/map';
import {KieServerConfig} from "../kieserver.config";

@Component({
  selector: 'app-scenario-detail',
  templateUrl: './scenario-detail.component.html',
  styleUrls: ['./scenario-detail.component.css']
})
export class ScenarioDetailComponent implements OnInit {

  scenarioId: string;

  constructor(private route: ActivatedRoute, @Inject('KieServerConfig') private kieServerConfig: KieServerConfig) {
    this.route.paramMap.map((params: ParamMap) =>
      params.get('scenarioId'))
      .subscribe((scenario: string) => {
        this.scenarioId = scenario;
      });
  }

  ngOnInit() {
  }


}
