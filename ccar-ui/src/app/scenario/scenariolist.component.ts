import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Scenario} from "./scenario";

@Component({
  selector: 'app-scenariolist',
  templateUrl: './scenariolist.component.html',
  styleUrls: ['./scenariolist.component.css']
})
export class ScenariolistComponent implements OnInit {

  items: any[];

  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get('/kie-server/services/rest/server/queries/processes/definitions',{
      headers: new HttpHeaders().set('Accept','application/json').set('ContentType','application/json')
    }).subscribe(data =>{
      this.items = data['processes'].map(p=>{
        var s: Scenario = {
          name: p['process-name'],
          id: p['process-id']
        }
        return s;
      }).filter(s => s.id.startsWith('scenarios'));
    });
  }

}
