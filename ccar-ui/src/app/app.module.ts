import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListModule, ActionModule} from 'patternfly-ng';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { ScenariolistComponent } from './scenario/scenariolist.component';
import { ScenarioDetailComponent } from './scenario-detail/scenario-detail.component';
import { ProcessimageComponent } from './processimage/processimage.component';
import {KieServerConfig} from "./kieserver.config";
import { SteplistComponent } from './steplist/steplist.component';
import { StepformComponent } from './stepform/stepform.component';

const appRoutes: Routes = [
  { path: '', component: ScenariolistComponent },
  { path: 'scenario/:scenarioId', component: ScenarioDetailComponent}
];

let kieserver: KieServerConfig = {
  containerId: 'rh.demo.ccar:scenarios:0.3.2'
}

@NgModule({
  declarations: [
    AppComponent,
    ScenariolistComponent,
    ScenarioDetailComponent,
    ProcessimageComponent,
    SteplistComponent,
    StepformComponent
  ],
  imports: [
    BrowserModule,
    ListModule,
    ActionModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [{provide: 'KieServerConfig', useValue:kieserver}],
  bootstrap: [AppComponent]
})
export class AppModule { }
